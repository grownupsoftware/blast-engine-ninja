![Alt text](http://www.gusl.co/blast/resources/images/b-circle-trans-100.png) *on* ![Alt text](http://www.ninjaframework.org/ninja_logo.png)

Built using Ninja version 6.0.0.rc1


## Jetty

If using Jetty, then add the Servlet from the Blast-Jetty-Engine.


```java
    public class ServletModule extends com.google.inject.servlet.ServletModule {
        @Override
        protected void configureServlets() {
            //   This could be bound ....
            BlastServer blastServer = Blast.blast(new EchoModule(), new PingModule(), new SimpleTopicModule());
            try {
                blastServer.startup();
                // We can now reference it.
                Blast.storeBlastInstance("blast.ninja", blastServer);
                JettyBlastServlet servlet = new JettyBlastServlet(blastServer);
                serve("/blast").with(servlet);
            } catch (BlastException ex) {
                System.err.println("Unable to start blast ");
                ex.printStackTrace();
            }
        }
    }
```

You will also need to add Jetty WebSockets to the Pom, build.gradle of the following.


```groovy
    compile "org.eclipse.jetty.websocket:websocket-servlet:${jettyVersion}"
    runtime "org.eclipse.jetty.websocket:websocket-api:${jettyVersion}"
    runtime "org.eclipse.jetty.websocket:websocket-server:${jettyVersion}"
```

## Tomcat

Just enable JSR356 annotations, include the Blast-Tomcat-Engine and away you go.

## Undertow

See how the Blast is used with undertow, and away you go.

## Gradle Runner
`./gradlew ninja:ninja-engine:run`
