/*
 * Grownup Software Limited.
 */
package conf;

import blast.Blast;
import blast.exception.BlastException;
import blast.jetty.engine.JettyBlastServlet;
import blast.module.echo.EchoModule;
import blast.module.ping.PingModule;
import blast.server.BlastServer;

/**
 *
 * @author dhudson - Apr 6, 2017 - 1:11:36 PM
 */
public class ServletModule extends com.google.inject.servlet.ServletModule {

    @Override
    protected void configureServlets() {
        BlastServer blastServer = Blast.blast(new EchoModule(), new PingModule());
        try {
            blastServer.startup();
            // We can now reference it.  Could be bound ....
            Blast.storeBlastInstance("blast.ninja", blastServer);
            JettyBlastServlet servlet = new JettyBlastServlet(blastServer);

            serve("/blast").with(servlet);
        } catch (BlastException ex) {
            System.err.println("Unable to start blast ");
            ex.printStackTrace();
        }
    }

}
